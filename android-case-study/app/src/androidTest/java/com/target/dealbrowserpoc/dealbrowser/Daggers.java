package com.target.dealbrowserpoc.dealbrowser;

import java.lang.reflect.Method;

public final class Daggers {
    private Daggers() {
        throw new AssertionError("No instances");
    }

    //Reflection based method that creates a component with its dependencies set, by reflection. Relies on
    //Dagger2 naming conventions. Only used in integration
    public static <T> T createComponent(Class<T> componentClass, Object... dependencies) {
        String fqn = componentClass.getName();

        String packageName = componentClass.getPackage().getName();
        // Accounts for inner classes, ie MyApplication$Component
        String simpleName = fqn.substring(packageName.length() + 1);
        String generatedName = (packageName + ".Dagger" + simpleName).replace('$', '_');

        try {
            Class<?> generatedClass = Class.forName(generatedName);
            Object builder = generatedClass.getMethod("builder").invoke(null);

            for (Method method : builder.getClass().getMethods()) {
                Class<?>[] params = method.getParameterTypes();
                if (params.length == 1) {
                    Class<?> dependencyClass = params[0];
                    for (Object dependency : dependencies) {
                        if (dependencyClass.isAssignableFrom(dependency.getClass())) {
                            method.invoke(builder, dependency);
                            break;
                        }
                    }
                }
            }
            //noinspection unchecked
            return (T) builder.getClass().getMethod("build").invoke(builder);
        } catch (Exception e) {
            String dependenciesErrorMessage = "";
            for (Object dependency : dependencies) {
                dependenciesErrorMessage = dependenciesErrorMessage + dependency.getClass().getSimpleName() + ", ";
            }
            String errorMessage = dependenciesErrorMessage.substring(0, dependenciesErrorMessage.length() - 2);
            throw new RuntimeException("Exception attempting to create component: " + componentClass.getSimpleName() + " with " + errorMessage, e);
        }
    }
}
