package com.target.dealbrowserpoc.dealbrowser.data.store;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import com.f2prateek.rx.preferences.RxSharedPreferences;
import com.target.dealbrowserpoc.dealbrowser.data.model.DealBrowserMode;
import org.junit.Before;
import org.junit.Test;
import rx.observers.TestSubscriber;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

public class PreferenceStoreTest {

    private final Context context = InstrumentationRegistry.getContext();
    private final SharedPreferences preferences = context.getSharedPreferences("test", Context.MODE_PRIVATE);

    private PreferenceStore preferenceStore;
    private TestSubscriber<DealBrowserMode> sub;

    @Before public void setUp() {
        preferences.edit().clear().apply();
        preferenceStore = new PreferenceStore(RxSharedPreferences.create(preferences));
        sub = new TestSubscriber<>();
    }

    @Test public void testBrowserModeDefaultsToList() {
        preferenceStore.dealBrowserMode().subscribe(sub);
        sub.assertReceivedOnNext(singletonList(DealBrowserMode.LIST));
        sub.assertNoTerminalEvent();
    }

    @Test public void testStoresNewBrowserModePreference() {
        preferenceStore.dealBrowserMode().subscribe(sub);
        preferenceStore.dealBrowserMode(DealBrowserMode.GRID);
        sub.assertReceivedOnNext(asList(DealBrowserMode.LIST, DealBrowserMode.GRID));
        sub.assertNoTerminalEvent();
    }
}