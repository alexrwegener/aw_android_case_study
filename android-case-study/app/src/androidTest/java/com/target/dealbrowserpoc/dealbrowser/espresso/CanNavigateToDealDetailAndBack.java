package com.target.dealbrowserpoc.dealbrowser.espresso;

import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import com.target.dealbrowserpoc.dealbrowser.MainActivity;
import com.target.dealbrowserpoc.dealbrowser.R;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.os.SystemClock.sleep;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.allOf;

@LargeTest @RunWith(AndroidJUnit4.class) public class CanNavigateToDealDetailAndBack {

    @Rule public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test public void canNavigateToDealDetailAndBack() {
        //Poor man's idling resource
        //In a fully featured debug app this would reference static data and load instantly
        sleep(SECONDS.toMillis(5));
        ViewInteraction actionMenuItemView = onView(allOf(ViewMatchers.withId(R.id.action_toggle_deal_browser), withContentDescription("List View"), isDisplayed()));

        actionMenuItemView.perform(click());
        sleep(SECONDS.toMillis(5));

        ViewInteraction recyclerView =
                onView(allOf(withId(R.id.deals_view_items), withParent(allOf(withId(R.id.deals_view), withParent(withId(R.id.container)))), isDisplayed()));
        recyclerView.perform(actionOnItemAtPosition(0, click()));
        //Waiting for transition (aka need to disable them on integration tests)
        sleep(SECONDS.toMillis(5));

        ViewInteraction appCompatImageButton = onView(allOf(withContentDescription("Navigate up"), withParent(withId(R.id.toolbar)), isDisplayed()));
        appCompatImageButton.perform(click());
        sleep(SECONDS.toMillis(5));

        ViewInteraction actionMenuItemView2 = onView(allOf(withId(R.id.action_toggle_deal_browser), withContentDescription("List View"), isDisplayed()));
        actionMenuItemView2.perform(click());
        sleep(SECONDS.toMillis(5));
    }
}
