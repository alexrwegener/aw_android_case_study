package com.target.dealbrowserpoc.dealbrowser.data.store;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import com.f2prateek.rx.preferences.RxSharedPreferences;
import com.google.gson.Gson;
import com.target.dealbrowserpoc.dealbrowser.Daggers;
import com.target.dealbrowserpoc.dealbrowser.DealBrowserComponent;
import com.target.dealbrowserpoc.dealbrowser.IntegrationScope;
import com.target.dealbrowserpoc.dealbrowser.MainActivity;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deals;
import com.target.dealbrowserpoc.dealbrowser.di.DaggerService;
import dagger.Component;
import javax.inject.Inject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import rx.observers.TestSubscriber;

import static java.util.Collections.singletonList;

public class DealsStoreTest {
    @Rule public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Inject Gson gson;
    private final Context context = InstrumentationRegistry.getContext();
    private final SharedPreferences preferences = context.getSharedPreferences("test", Context.MODE_PRIVATE);
    private final Deal deal = Deal.create("id", "imageUrl", "aisle", "description", "guid", "price", "title", "salePrice");
    private DealsStore dealsStore;
    private TestSubscriber<Deal> dealSub;
    private TestSubscriber<Deals> dealsSub;

    @Before public void before() {
        DealBrowserComponent appComponent = DaggerService.getAppComponent(activityRule.getActivity());
        Daggers.createComponent(TestComponent.class, appComponent).inject(this);
        preferences.edit().clear().apply();
        dealsStore = new DealsStore(RxSharedPreferences.create(preferences), gson);
        dealSub = new TestSubscriber<>();
        dealsSub = new TestSubscriber<>();
    }

    @Test public void testActiveDealDefaultsToNull() {
        dealsStore.activeDeal().subscribe(dealSub);
        dealSub.assertReceivedOnNext(singletonList(null));
        dealSub.assertNoTerminalEvent();
    }

    @Test public void testDealsDefaultsToNull() {
        dealsStore.deals().subscribe(dealsSub);
        dealsSub.assertReceivedOnNext(singletonList(null));
        dealsSub.assertNoTerminalEvent();
    }

    @Test public void testStoresNewDeals() {
        Deals deals = Deals.create(singletonList(deal));
        dealsStore.deals(deals);
        dealsStore.deals().subscribe(dealsSub);
        dealsSub.assertReceivedOnNext(singletonList(deals));
        dealsSub.assertNoTerminalEvent();
    }

    @Test public void testStoresNewActiveDeal() {
        dealsStore.activeDeal(deal);
        dealsStore.activeDeal().subscribe(dealSub);
        dealSub.assertReceivedOnNext(singletonList(deal));
        dealSub.assertNoTerminalEvent();
    }

    @IntegrationScope @Component(dependencies = DealBrowserComponent.class) interface TestComponent {
        void inject(DealsStoreTest test);
    }
}