package com.target.dealbrowserpoc.dealbrowser;

import com.target.dealbrowserpoc.dealbrowser.data.ReleaseDataModule;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import com.target.dealbrowserpoc.dealbrowser.interactor.ReleaseInteractorModule;
import dagger.Component;

@ApplicationScope @Component(modules = { DealBrowserModule.class, ReleaseDataModule.class, ReleaseInteractorModule.class })
public interface DealBrowserComponent extends AppComponent {
    final class Initializer {
        private Initializer() {
            //No op
        }

        static DealBrowserComponent init(DealBrowserApp app) {
            return DaggerDealBrowserComponent.builder().dealBrowserModule(new DealBrowserModule(app)).build();
        }
    }
}