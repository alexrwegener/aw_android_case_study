package com.target.dealbrowserpoc.dealbrowser.data;

import android.app.Application;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

import static com.target.dealbrowserpoc.dealbrowser.data.DataModule.createOkHttpClient;

@Module public class ReleaseDataModule {
    @Provides @ApplicationScope public OkHttpClient provideOkHttpClient(Application app) {
        return createOkHttpClient(app).build();
    }

    @Provides @ApplicationScope public MemoryLeakDetector provideMemoryLeakDetector() {
        return MemoryLeakDetector.NOOP;
    }
}
