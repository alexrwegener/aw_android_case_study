package com.target.dealbrowserpoc.dealbrowser;

import com.google.gson.Gson;
import com.target.dealbrowserpoc.dealbrowser.ui.deal.DealController;
import com.target.dealbrowserpoc.dealbrowser.ui.deals.DealsController;

public interface AppComponent {
    void inject(DealsController controller);

    void inject(DealController dealController);

    Gson provideGson();
}
