package com.target.dealbrowserpoc.dealbrowser.ui.changehandler;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.NonNull;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import com.bluelinelabs.conductor.changehandler.TransitionChangeHandler;
import com.target.dealbrowserpoc.dealbrowser.R;

@TargetApi(Build.VERSION_CODES.LOLLIPOP) public final class ImageDetailChangeHandler extends TransitionChangeHandler {

    @Override @NonNull protected Transition getTransition(@NonNull ViewGroup container, View from, View to, boolean isPush) {
        TransitionSet transition = new TransitionSet().setOrdering(TransitionSet.ORDERING_SEQUENTIAL);
        transition.addTransition(new Fade(Fade.OUT).addTarget(from));
        View fromImage = ButterKnife.findById(from, R.id.deal_item_image);
        View toImage = ButterKnife.findById(to, R.id.deal_item_image);
        if (fromImage != null && toImage != null && Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            transition.addTransition(new TransitionSet().setOrdering(TransitionSet.ORDERING_TOGETHER)
                    .addTransition(new ChangeBounds())
                    .addTransition(new ChangeTransform())
                    .addTransition(new ChangeImageTransform()));
        }
        transition.addTransition(new Fade(Fade.IN).addTarget(to));
        return transition;
    }
}