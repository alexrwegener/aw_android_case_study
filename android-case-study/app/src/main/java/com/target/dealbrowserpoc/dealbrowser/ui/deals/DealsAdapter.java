package com.target.dealbrowserpoc.dealbrowser.ui.deals;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.squareup.picasso.Picasso;
import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;
import com.target.dealbrowserpoc.dealbrowser.data.model.DealBrowserMode;
import java.util.List;
import rx.functions.Action2;

import static java.util.Collections.emptyList;

class DealsAdapter extends RecyclerView.Adapter<DealViewHolder> implements Action2<List<DealRenderModel>, DealBrowserMode> {
    private static final int GRID_VIEW_TYPE = 1;
    private static final int LIST_VIEW_TYPE = 2;
    private final Picasso picasso;
    private final DealClickedListener dealClickedListener;
    private List<DealRenderModel> deals = emptyList();
    private DealBrowserMode mode;

    DealsAdapter(Picasso picasso, DealClickedListener dealClickedListener) {
        this.picasso = picasso;
        this.dealClickedListener = dealClickedListener;
    }

    @Override public int getItemViewType(int position) {
        if (mode.isGrid()) {
            return GRID_VIEW_TYPE;
        } else {
            return LIST_VIEW_TYPE;
        }
    }

    @Override public DealViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (GRID_VIEW_TYPE == viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.deal_grid_item, parent, false);
            return new GridViewHolder(view, picasso, dealClickedListener);
        } else if (LIST_VIEW_TYPE == viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.deal_list_item, parent, false);
            return new ListViewHolder(view, picasso, dealClickedListener);
        } else {
            throw new IllegalArgumentException(String.format("Unknown view type: %s", viewType));
        }
    }

    @Override public void onBindViewHolder(DealViewHolder holder, int position) {
        holder.render(deals.get(position));
    }

    @Override public int getItemCount() {
        return deals.size();
    }

    @Override public void call(List<DealRenderModel> deals, DealBrowserMode dealBrowserMode) {
        this.deals = deals;
        this.mode = dealBrowserMode;
        notifyDataSetChanged();
    }

    interface DealClickedListener {
        void onDealClicked(Deal deal);

        void onDealShippedClicked(Deal deal);

        void onDealAisleClicked(Deal deal);
    }
}