package com.target.dealbrowserpoc.dealbrowser.ui.deal;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import com.squareup.picasso.Picasso;
import com.target.dealbrowserpoc.dealbrowser.DealBrowserComponent;
import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;
import com.target.dealbrowserpoc.dealbrowser.interactor.deal.DealsInteractor;
import com.target.dealbrowserpoc.dealbrowser.ui.BaseController;
import com.target.dealbrowserpoc.dealbrowser.ui.util.Snackbars;
import com.target.dealbrowserpoc.dealbrowser.ui.util.Truss;
import com.target.dealbrowserpoc.dealbrowser.util.Strings;
import javax.inject.Inject;
import rx.Subscription;
import timber.log.Timber;

import static com.target.dealbrowserpoc.dealbrowser.ui.util.Snackbars.safeDismiss;
import static com.target.dealbrowserpoc.dealbrowser.ui.util.Subscriptions.safeUnsubscribe;
import static com.target.dealbrowserpoc.dealbrowser.ui.util.Transformers.mapDistinctToMainThread;
import static com.target.dealbrowserpoc.dealbrowser.util.Strings.isBlank;

public class DealController extends BaseController {
    @Inject DealsInteractor dealsInteractor;
    @Inject Picasso picasso;

    @BindView(R.id.deal_item_image) ImageView image;
    @BindView(R.id.deal_view_price) TextView price;
    @BindView(R.id.deal_view_regular_price) TextView regularPrice;
    @BindView(R.id.deal_view_description) TextView description;
    @BindView(R.id.deal_view_title) TextView title;
    @BindString(R.string.reg_abbreviation) String regAbbr;

    private Snackbar snackbar;
    private Subscription sub;

    public DealController() {
        super();
        setHasOptionsMenu(true);
    }

    static DealRenderModel toRenderModel(Deal deal) {
        boolean hasSalePrice = !isBlank(deal.salePrice());
        return DealRenderModel.create(Strings.titleize(deal.title()), deal.description(), deal.image(), hasSalePrice,
                hasSalePrice ? deal.salePrice() : deal.price(), deal.price());
    }

    @Override protected View inflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        return inflater.inflate(R.layout.deal_view, container, false);
    }

    @Override protected void onViewBound(@NonNull View view) {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("");
        }
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getRouter().handleBack();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override protected void onAttach(@NonNull View view) {
        source();
    }

    @Override protected void onDetach(@NonNull View view) {
        safeUnsubscribe(sub);
        safeDismiss(snackbar);
    }

    @Override protected void inject(DealBrowserComponent component) {
        component.inject(this);
    }

    @Override protected void onDestroyView(View view) {
        super.onDestroyView(view);
        snackbar = null;
    }

    void source() {
        sub = dealsInteractor.activeDeal().compose(mapDistinctToMainThread(DealController::toRenderModel)).subscribe(this::render, Timber::e);
    }

    void render(DealRenderModel model) {
        picasso.load(model.imageUrl()).into(image);
        title.setText(model.title());
        description.setText(model.description());
        price.setText(model.price());
        regularPrice.setVisibility(model.hasSalePrice() ? View.VISIBLE : View.GONE);

        Truss regularPriceSpan = new Truss();
        regularPriceSpan.append(regAbbr);
        regularPriceSpan.append(' ');
        regularPriceSpan.pushSpan(new StrikethroughSpan());
        regularPriceSpan.append(model.regularPrice());
        regularPriceSpan.popSpan();
        regularPrice.setText(regularPriceSpan.build());
    }

    @OnClick(R.id.deal_view_add_to_list) void addToList(View view) {
        safeDismiss(snackbar);
        snackbar = Snackbars.showMessage(view, "Add to list ʘ‿ʘ");
    }

    @OnClick(R.id.deal_view_add_to_cart) void addToCart(View view) {
        safeDismiss(snackbar);
        snackbar = Snackbars.showMessage(view, "Add to cart (ᵔᴥᵔ)");
    }
}
