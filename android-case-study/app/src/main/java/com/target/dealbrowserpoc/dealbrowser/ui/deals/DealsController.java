package com.target.dealbrowserpoc.dealbrowser.ui.deals;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindDimen;
import butterknife.BindView;
import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler;
import com.squareup.picasso.Picasso;
import com.target.dealbrowserpoc.dealbrowser.DealBrowserComponent;
import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;
import com.target.dealbrowserpoc.dealbrowser.data.model.DealBrowserMode;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deals;
import com.target.dealbrowserpoc.dealbrowser.interactor.deal.DealsInteractor;
import com.target.dealbrowserpoc.dealbrowser.interactor.preference.PreferenceInteractor;
import com.target.dealbrowserpoc.dealbrowser.ui.BaseController;
import com.target.dealbrowserpoc.dealbrowser.ui.changehandler.ImageDetailChangeHandlerCompat;
import com.target.dealbrowserpoc.dealbrowser.ui.deal.DealController;
import com.target.dealbrowserpoc.dealbrowser.ui.util.Snackbars;
import com.target.dealbrowserpoc.dealbrowser.ui.widget.AutoFitGridLayoutManager;
import com.target.dealbrowserpoc.dealbrowser.util.Strings;
import java.util.List;
import javax.inject.Inject;
import rx.Subscription;
import timber.log.Timber;

import static com.target.dealbrowserpoc.dealbrowser.ui.util.Lists.transform;
import static com.target.dealbrowserpoc.dealbrowser.ui.util.Snackbars.safeDismiss;
import static com.target.dealbrowserpoc.dealbrowser.ui.util.Subscriptions.safeUnsubscribe;
import static com.target.dealbrowserpoc.dealbrowser.ui.util.Transformers.distinctToMainThread;
import static com.target.dealbrowserpoc.dealbrowser.util.Strings.isBlank;
import static java.util.Collections.emptyList;
import static rx.Observable.combineLatest;

public class DealsController extends BaseController implements DealsAdapter.DealClickedListener {

    @Inject DealsInteractor dealsInteractor;
    @Inject PreferenceInteractor preferenceInteractor;
    @Inject Picasso picasso;

    @BindView(R.id.deals_view_loading) View loading;
    @BindView(R.id.deals_view_items) RecyclerView deals;
    @BindView(R.id.deals_view) View view;
    @BindDimen(R.dimen.deal_grid_item_image_size) int gridDealWidth;

    private DealsRenderModel model;
    private AutoFitGridLayoutManager layoutManager;
    private DealsAdapter adapter;
    private Subscription sub;
    private Snackbar snackbar;
    private MenuItem browserModeMenuItem;

    public DealsController() {
        super();
        setHasOptionsMenu(true);
    }

    static DealsRenderModel toRenderModel(Deals deals, DealBrowserMode mode) {
        List<DealRenderModel> data = deals == null ? emptyList() : transform(deals.data(), DealsController::toDealRenderModel);
        return DealsRenderModel.create(data, mode);
    }

    static DealRenderModel toDealRenderModel(Deal deal) {
        boolean hasSalePrice = !isBlank(deal.salePrice());
        return DealRenderModel.create(deal, Strings.titleize(deal.title()), deal.image(), hasSalePrice ? deal.salePrice() : deal.price(),
                Strings.capitalize(deal.aisle()));
    }

    @Override protected View inflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        return inflater.inflate(R.layout.deals_view, container, false);
    }

    @Override protected void onViewBound(@NonNull View view) {
        adapter = new DealsAdapter(picasso, this);
        layoutManager = new AutoFitGridLayoutManager(view.getContext(), gridDealWidth, LinearLayoutManager.VERTICAL, false);
        deals.setHasFixedSize(true);
        deals.setLayoutManager(layoutManager);
        deals.setAdapter(adapter);
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setTitle(R.string.app_name);
        }
    }

    @Override protected void inject(DealBrowserComponent component) {
        component.inject(this);
    }

    @Override protected void onAttach(@NonNull View view) {
        source();
    }

    @Override protected void onDetach(@NonNull View view) {
        safeUnsubscribe(sub);
        safeDismiss(snackbar);
    }

    @Override protected void onDestroyView(View view) {
        super.onDestroyView(view);
        snackbar = null;
        browserModeMenuItem = null;
        layoutManager = null;
        adapter = null;
    }

    void source() {
        safeUnsubscribe(sub);
        sub = combineLatest(dealsInteractor.deals(), preferenceInteractor.dealBrowserMode(), DealsController::toRenderModel).compose(distinctToMainThread())
                .subscribe(this::render, throwable -> {
                    Timber.e(throwable, "Unable to source %s: %s", DealsController.class.getSimpleName(), throwable.getMessage());
                    if (view == null) {
                        return;
                    }
                    renderLoading(false);
                    Resources resources = view.getContext().getResources();
                    snackbar = Snackbars.showMessageWithAction(view, resources.getString(R.string.network_error), resources.getString(R.string.retry),
                            (view) -> source());
                });
    }

    void render(DealsRenderModel model) {
        this.model = model;
        browserModeMenuItem.setIcon(model.dealBrowserMode().iconId);
        renderLoading(model.deals().isEmpty());

        int listWidth = deals.getContext().getResources().getDisplayMetrics().widthPixels;
        layoutManager.setColumnWidth(model.dealBrowserMode().isList() ? listWidth : gridDealWidth);
        adapter.call(model.deals(), model.dealBrowserMode());
    }

    void renderLoading(boolean isLoading) {
        loading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        deals.setVisibility(isLoading ? View.GONE : View.VISIBLE);
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.deals_menu, menu);
        browserModeMenuItem = menu.findItem(R.id.action_toggle_deal_browser);
        if (model != null) {
            browserModeMenuItem.setIcon(model.dealBrowserMode().iconId);
        }
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_toggle_deal_browser) {
            preferenceInteractor.toggleDealBrowserMode(model.dealBrowserMode());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override public void onDealClicked(Deal deal) {
        dealsInteractor.activeDeal(deal);
        getRouter().pushController(RouterTransaction.with(new DealController())
                .pushChangeHandler(new ImageDetailChangeHandlerCompat())
                .popChangeHandler(new HorizontalChangeHandler()));
    }

    @Override public void onDealShippedClicked(Deal deal) {
        safeDismiss(snackbar);
        snackbar = Snackbars.showMessage(view, "Ship deal ʘ‿ʘ");
    }

    @Override public void onDealAisleClicked(Deal deal) {
        safeDismiss(snackbar);
        snackbar = Snackbars.showMessage(view, "Show item on store map (^ ^)");
    }
}
