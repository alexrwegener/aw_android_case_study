package com.target.dealbrowserpoc.dealbrowser.data.api;

import com.google.gson.TypeAdapterFactory;
import com.ryanharter.auto.value.gson.GsonTypeAdapterFactory;

//Adds all auto-gson generated type adapters to type adapter factory
@GsonTypeAdapterFactory abstract class DealBrowserTypeAdapterFactory implements TypeAdapterFactory {
    static TypeAdapterFactory create() {
        return new AutoValueGson_DealBrowserTypeAdapterFactory();
    }
}