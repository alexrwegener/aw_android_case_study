package com.target.dealbrowserpoc.dealbrowser.ui.deals;

import android.support.v7.widget.RecyclerView;
import android.view.View;

abstract class DealViewHolder extends RecyclerView.ViewHolder {

    public DealViewHolder(View itemView) {
        super(itemView);
    }

    abstract void render(DealRenderModel deal);
}