package com.target.dealbrowserpoc.dealbrowser.data;

//Indirection to add LeakCanary in debug
public interface MemoryLeakDetector {
    MemoryLeakDetector NOOP = leakable -> {
    };

    void watch(Object leakable);
}