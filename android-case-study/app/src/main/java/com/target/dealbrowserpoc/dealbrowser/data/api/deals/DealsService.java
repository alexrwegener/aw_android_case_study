package com.target.dealbrowserpoc.dealbrowser.data.api.deals;

import com.target.dealbrowserpoc.dealbrowser.data.model.Deals;
import retrofit2.http.GET;
import rx.Observable;

public interface DealsService {
    @GET("deals") Observable<Deals> getDeals();
}
