package com.target.dealbrowserpoc.dealbrowser.interactor.deal;

import com.target.dealbrowserpoc.dealbrowser.data.api.deals.DealsService;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deals;
import com.target.dealbrowserpoc.dealbrowser.data.store.DealsStore;
import rx.Observable;

public final class RealDealsInteractor implements DealsInteractor {
    private final DealsService service;
    private final DealsStore store;

    public RealDealsInteractor(DealsService service, DealsStore store) {
        this.service = service;
        this.store = store;
    }

    @Override public Observable<Deals> deals() {
        return service.getDeals().concatWith(store.deals()).doOnNext(store::deals);
    }

    @Override public Observable<Deal> activeDeal() {
        return store.activeDeal();
    }

    @Override public void activeDeal(Deal deal) {
        store.activeDeal(deal);
    }
}
