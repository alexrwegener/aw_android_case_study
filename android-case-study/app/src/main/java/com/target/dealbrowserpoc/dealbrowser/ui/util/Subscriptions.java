package com.target.dealbrowserpoc.dealbrowser.ui.util;

import rx.Subscription;

public final class Subscriptions {
    private Subscriptions() {
        throw new AssertionError("No instances");
    }

    public static void safeUnsubscribe(Subscription sub) {
        if (sub != null && !sub.isUnsubscribed()) {
            sub.unsubscribe();
        }
    }
}
