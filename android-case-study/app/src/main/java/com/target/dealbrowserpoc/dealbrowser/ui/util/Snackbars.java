package com.target.dealbrowserpoc.dealbrowser.ui.util;

import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import com.target.dealbrowserpoc.dealbrowser.R;

public final class Snackbars {
    private Snackbars() {
        throw new AssertionError("No instances");
    }

    public static Snackbar showMessage(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.show();
        return snackbar;
    }

    public static Snackbar showMessageWithAction(View view, String message, String actionMessage, View.OnClickListener listener) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.setAction(actionMessage, listener);
        snackbar.setActionTextColor(ContextCompat.getColor(view.getContext(), R.color.snackbar_action));
        snackbar.show();
        return snackbar;
    }

    public static void safeDismiss(Snackbar snackbar) {
        if (snackbar == null || !snackbar.isShownOrQueued()) {
            return;
        }
        snackbar.dismiss();
    }
}
