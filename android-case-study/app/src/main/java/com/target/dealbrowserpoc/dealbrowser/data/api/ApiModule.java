package com.target.dealbrowserpoc.dealbrowser.data.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.target.dealbrowserpoc.dealbrowser.BuildConfig;
import com.target.dealbrowserpoc.dealbrowser.data.api.deals.DealsService;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

@Module public final class ApiModule {

    public static final HttpUrl PRD_DEALS_API = HttpUrl.parse(BuildConfig.DEALS_URL);

    //Would be declared in debug / release for apps with multiple environments
    @Provides @ApplicationScope HttpUrl provideBaseUrl() {
        return PRD_DEALS_API;
    }

    @Provides @ApplicationScope Gson provideGson() {
        return new GsonBuilder().registerTypeAdapterFactory(DealBrowserTypeAdapterFactory.create()).create();
    }

    @Provides @ApplicationScope Retrofit provideRetrofit(HttpUrl baseUrl, OkHttpClient client, Gson gson) {
        return new Retrofit.Builder().client(client)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
    }

    @Provides @ApplicationScope DealsService provideDealsService(Retrofit retrofit) {
        return retrofit.create(DealsService.class);
    }
}
