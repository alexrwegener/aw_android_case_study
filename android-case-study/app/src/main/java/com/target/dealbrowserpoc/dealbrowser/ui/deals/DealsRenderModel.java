package com.target.dealbrowserpoc.dealbrowser.ui.deals;

import com.google.auto.value.AutoValue;
import com.target.dealbrowserpoc.dealbrowser.data.model.DealBrowserMode;
import java.util.List;

@AutoValue abstract class DealsRenderModel {
    static DealsRenderModel create(List<DealRenderModel> deals, DealBrowserMode dealBrowserMode) {
        return new AutoValue_DealsRenderModel(deals, dealBrowserMode);
    }

    abstract List<DealRenderModel> deals();

    abstract DealBrowserMode dealBrowserMode();
}
