package com.target.dealbrowserpoc.dealbrowser.ui.deal;

import com.google.auto.value.AutoValue;

@AutoValue abstract class DealRenderModel {
    static DealRenderModel create(String title, String description, String imageUrl, boolean hasSalePrice, String price, String regularPrice) {
        return new AutoValue_DealRenderModel(title, description, imageUrl, hasSalePrice, price, regularPrice);
    }

    abstract String title();

    abstract String description();

    abstract String imageUrl();

    abstract boolean hasSalePrice();

    abstract String price();

    abstract String regularPrice();
}
