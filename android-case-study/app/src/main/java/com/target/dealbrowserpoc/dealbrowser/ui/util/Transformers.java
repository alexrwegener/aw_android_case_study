package com.target.dealbrowserpoc.dealbrowser.ui.util;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public final class Transformers {
    private Transformers() {
        throw new AssertionError("No instances");
    }

    public static <T> Observable.Transformer<T, T> distinctToMainThread() {
        return obs -> obs.subscribeOn(Schedulers.computation()).distinctUntilChanged().observeOn(AndroidSchedulers.mainThread());
    }

    public static <T, R> Observable.Transformer<T, R> mapDistinctToMainThread(Func1<T, R> mapFunc) {
        return obs -> obs.subscribeOn(Schedulers.computation()).map(mapFunc).distinctUntilChanged().observeOn(AndroidSchedulers.mainThread());
    }
}
