package com.target.dealbrowserpoc.dealbrowser.interactor.deal;

import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deals;
import rx.Observable;

//Indirection to setup u2020 style dev tools
public interface DealsInteractor {

    Observable<Deals> deals();

    Observable<Deal> activeDeal();

    void activeDeal(Deal deal);
}
