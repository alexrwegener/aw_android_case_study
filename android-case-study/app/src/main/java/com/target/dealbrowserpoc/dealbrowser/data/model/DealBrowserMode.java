package com.target.dealbrowserpoc.dealbrowser.data.model;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import com.target.dealbrowserpoc.dealbrowser.R;

public enum DealBrowserMode {
    LIST(R.drawable.ic_view_list_white_24dp, R.string.list_view),
    GRID(R.drawable.ic_view_module_white_24dp, R.string.grid_view);

    @DrawableRes public final int iconId;
    @StringRes public final int titleId;

    DealBrowserMode(@DrawableRes int iconId, @StringRes int titleId) {
        this.iconId = iconId;
        this.titleId = titleId;
    }

    public boolean isList() {
        return this == LIST;
    }

    public boolean isGrid() {
        return this == GRID;
    }

    public DealBrowserMode toggle() {
        if (isList()) {
            return GRID;
        } else if (isGrid()) {
            return LIST;
        } else {
            throw new IllegalArgumentException(String.format("Unknown mode: %s", name()));
        }
    }
}
