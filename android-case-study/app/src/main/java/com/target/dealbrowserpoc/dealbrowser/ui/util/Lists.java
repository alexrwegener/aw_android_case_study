package com.target.dealbrowserpoc.dealbrowser.ui.util;

import java.util.ArrayList;
import java.util.List;
import rx.functions.Func1;

public final class Lists {
    private Lists() {
        throw new AssertionError("No instances");
    }

    public static <T, R> List<R> transform(List<T> list, Func1<T, R> func) {
        List<R> transformed = new ArrayList<>(list.size());
        for (T item : list) {
            transformed.add(func.call(item));
        }
        return transformed;
    }
}