package com.target.dealbrowserpoc.dealbrowser.data.store;

import com.f2prateek.rx.preferences.Preference;
import com.f2prateek.rx.preferences.RxSharedPreferences;
import com.google.gson.Gson;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deals;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import javax.inject.Inject;
import rx.Observable;

//Could be backed via database or file store. Used shared preferences for brevity and knowledge of a fixed deals response
@ApplicationScope public class DealsStore {
    private final Preference<Deals> dealsPref;
    private final Preference<Deal> activeDealPref;

    @Inject DealsStore(RxSharedPreferences prefs, Gson gson) {
        this.dealsPref = prefs.getObject("deals", new GsonPreferenceAdapter<>(gson, Deals.class));
        this.activeDealPref = prefs.getObject("active_deal", new GsonPreferenceAdapter<>(gson, Deal.class));
    }

    public Observable<Deals> deals() {
        return dealsPref.asObservable();
    }

    public void deals(Deals deals) {
        dealsPref.set(deals);
    }

    public Observable<Deal> activeDeal() {
        return activeDealPref.asObservable();
    }

    public void activeDeal(Deal deal) {
        activeDealPref.set(deal);
    }
}
