package com.target.dealbrowserpoc.dealbrowser.di;

import android.content.Context;
import com.target.dealbrowserpoc.dealbrowser.DealBrowserComponent;

public final class DaggerService {
    public static final String SERVICE_NAME = DaggerService.class.getName();

    private DaggerService() {
        throw new AssertionError("No instances");
    }

    @SuppressWarnings("WrongConstant") public static DealBrowserComponent getAppComponent(Context context) {
        return (DealBrowserComponent) context.getApplicationContext().getSystemService(SERVICE_NAME);
    }
}