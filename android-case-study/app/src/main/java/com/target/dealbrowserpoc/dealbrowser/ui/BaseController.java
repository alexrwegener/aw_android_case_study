package com.target.dealbrowserpoc.dealbrowser.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.bluelinelabs.conductor.Controller;
import com.target.dealbrowserpoc.dealbrowser.DealBrowserComponent;
import com.target.dealbrowserpoc.dealbrowser.data.MemoryLeakDetector;
import com.target.dealbrowserpoc.dealbrowser.di.DaggerService;
import javax.inject.Inject;

public abstract class BaseController extends Controller {
    @Inject MemoryLeakDetector memoryLeakDetector;
    private Unbinder unbinder;
    private boolean injected;

    protected BaseController() {
        this(null);
    }

    protected BaseController(Bundle args) {
        super(args);
    }

    protected abstract View inflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container);

    protected void onViewBound(@NonNull View view) {
    }

    protected abstract void inject(DealBrowserComponent component);

    @NonNull @Override protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        if (!injected) {
            inject(DaggerService.getAppComponent(container.getContext()));
            injected = true;
        }
        View view = inflateView(inflater, container);
        unbinder = ButterKnife.bind(this, view);
        onViewBound(view);
        return view;
    }

    @Override protected void onDestroyView(View view) {
        super.onDestroyView(view);
        unbinder.unbind();
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        if (memoryLeakDetector != null) {
            memoryLeakDetector.watch(this);
        }
    }

    @Nullable protected ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }
}
