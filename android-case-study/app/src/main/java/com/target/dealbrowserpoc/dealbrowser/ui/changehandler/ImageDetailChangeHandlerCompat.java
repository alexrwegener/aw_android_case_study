package com.target.dealbrowserpoc.dealbrowser.ui.changehandler;

import com.bluelinelabs.conductor.changehandler.FadeChangeHandler;
import com.bluelinelabs.conductor.changehandler.TransitionChangeHandlerCompat;

public final class ImageDetailChangeHandlerCompat extends TransitionChangeHandlerCompat {

    public ImageDetailChangeHandlerCompat() {
        super(new ImageDetailChangeHandler(), new FadeChangeHandler());
    }
}
