package com.target.dealbrowserpoc.dealbrowser;

import android.app.Application;
import android.os.StrictMode;
import com.target.dealbrowserpoc.dealbrowser.di.DaggerService;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static android.os.StrictMode.setThreadPolicy;
import static android.os.StrictMode.setVmPolicy;

public class DealBrowserApp extends Application {
    private static DealBrowserComponent appComponent;

    public static DealBrowserComponent getAppComponent() {
        return appComponent;
    }

    @Override public void onCreate() {
        if (BuildConfig.DEBUG) {
            setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
            setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());
        }
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        //Normally add production logging / crash reporting trees
        Timber.i("Initializing App");
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder().setDefaultFontPath(getString(R.string._font_name_pn_regular)).setFontAttrId(R.attr.fontPath).build());
        buildComponentAndInject();
    }

    @Override public Object getSystemService(String name) {
        if (name.equals(DaggerService.SERVICE_NAME) && appComponent != null) {
            return appComponent;
        }
        return super.getSystemService(name);
    }

    private void buildComponentAndInject() {
        appComponent = DealBrowserComponent.Initializer.init(this);
    }
}
