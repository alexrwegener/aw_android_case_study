package com.target.dealbrowserpoc.dealbrowser.data.store;

import com.f2prateek.rx.preferences.Preference;
import com.f2prateek.rx.preferences.RxSharedPreferences;
import com.target.dealbrowserpoc.dealbrowser.data.model.DealBrowserMode;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import javax.inject.Inject;
import rx.Observable;

@ApplicationScope public class PreferenceStore {
    private final Preference<DealBrowserMode> listModePref;

    @Inject PreferenceStore(RxSharedPreferences prefs) {
        this.listModePref = prefs.getEnum("list_mode", DealBrowserMode.LIST, DealBrowserMode.class);
    }

    public Observable<DealBrowserMode> dealBrowserMode() {
        return listModePref.asObservable();
    }

    public void dealBrowserMode(DealBrowserMode dealBrowserMode) {
        listModePref.set(dealBrowserMode);
    }
}