package com.target.dealbrowserpoc.dealbrowser.data.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import java.util.List;

@AutoValue public abstract class Deals {
    public static Deals create(List<Deal> data) {
        return new AutoValue_Deals(data);
    }

    public static TypeAdapter<Deals> typeAdapter(Gson gson) {
        return new AutoValue_Deals.GsonTypeAdapter(gson);
    }

    public abstract List<Deal> data();
}