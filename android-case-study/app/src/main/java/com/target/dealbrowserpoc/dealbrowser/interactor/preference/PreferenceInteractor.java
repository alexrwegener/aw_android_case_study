package com.target.dealbrowserpoc.dealbrowser.interactor.preference;

import com.target.dealbrowserpoc.dealbrowser.data.model.DealBrowserMode;
import com.target.dealbrowserpoc.dealbrowser.data.store.PreferenceStore;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import javax.inject.Inject;
import rx.Observable;

@ApplicationScope public class PreferenceInteractor {

    private final PreferenceStore store;

    @Inject PreferenceInteractor(PreferenceStore store) {
        this.store = store;
    }

    public Observable<DealBrowserMode> dealBrowserMode() {
        return store.dealBrowserMode();
    }

    public void toggleDealBrowserMode(DealBrowserMode mode) {
        store.dealBrowserMode(mode.toggle());
    }
}