package com.target.dealbrowserpoc.dealbrowser.ui.deals;

import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.squareup.picasso.Picasso;
import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;

class GridViewHolder extends DealViewHolder {
    @BindView(R.id.deal_grid_item_image) ImageView image;
    @BindView(R.id.deal_grid_item_title) TextView price;
    @BindString(R.string.deal_item_image_transition) String dealDetailTransition;
    private final Picasso picasso;
    private final DealsAdapter.DealClickedListener dealClickedListener;

    GridViewHolder(View view, Picasso picasso, DealsAdapter.DealClickedListener dealClickedListener) {
        super(view);
        this.picasso = picasso;
        this.dealClickedListener = dealClickedListener;
        ButterKnife.bind(this, view);
    }

    @Override public void render(DealRenderModel model) {
        picasso.load(model.imageUrl()).into(image);
        price.setText(model.price());
        itemView.setTag(model.deal());
    }

    @OnClick(R.id.deal_grid_item) void onClick(View view) {
        image.setId(R.id.deal_item_image);
        ViewCompat.setTransitionName(image, dealDetailTransition);
        dealClickedListener.onDealClicked((Deal) view.getTag());
    }
}