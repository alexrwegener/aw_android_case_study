package com.target.dealbrowserpoc.dealbrowser.ui.deals;

import com.google.auto.value.AutoValue;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;

@AutoValue abstract class DealRenderModel {
    static DealRenderModel create(Deal deal, String title, String imageUrl, String price, String aisle) {
        return new AutoValue_DealRenderModel(deal, title, imageUrl, price, aisle);
    }

    abstract Deal deal();

    abstract String title();

    abstract String imageUrl();

    abstract String price();

    abstract String aisle();
}