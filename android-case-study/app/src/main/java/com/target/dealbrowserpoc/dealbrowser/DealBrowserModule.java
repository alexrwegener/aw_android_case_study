package com.target.dealbrowserpoc.dealbrowser;

import android.app.Application;
import com.target.dealbrowserpoc.dealbrowser.data.DataModule;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import dagger.Module;
import dagger.Provides;

@Module(includes = { DataModule.class }) public final class DealBrowserModule {
    private final DealBrowserApp app;

    public DealBrowserModule(DealBrowserApp app) {
        this.app = app;
    }

    @Provides @ApplicationScope Application provideApplication() {
        return app;
    }
}