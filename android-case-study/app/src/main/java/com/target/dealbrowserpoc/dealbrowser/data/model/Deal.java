package com.target.dealbrowserpoc.dealbrowser.data.model;

import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue public abstract class Deal {
    public static Deal create(String id, String imageUrl, String aisle, String description, String guid, String price, String title, String salePrice) {
        return new AutoValue_Deal(id, imageUrl, aisle, description, guid, price, title, salePrice);
    }

    public static TypeAdapter<Deal> typeAdapter(Gson gson) {
        return new AutoValue_Deal.GsonTypeAdapter(gson);
    }

    @SerializedName("_id") public abstract String id();

    public abstract String image();

    public abstract String aisle();

    public abstract String description();

    public abstract String guid();

    public abstract String price();

    public abstract String title();

    @Nullable public abstract String salePrice();
}
