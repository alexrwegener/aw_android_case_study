package com.target.dealbrowserpoc.dealbrowser.ui.deals;

import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.squareup.picasso.Picasso;
import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;

class ListViewHolder extends DealViewHolder {
    @BindView(R.id.deal_list_item_image) ImageView image;
    @BindView(R.id.deal_list_item_title) TextView title;
    @BindView(R.id.deal_list_item_price) TextView price;
    @BindView(R.id.deal_list_item_aisle) TextView aisle;
    @BindString(R.string.deal_item_image_transition) String dealDetailTransition;

    private final Picasso picasso;
    private final DealsAdapter.DealClickedListener dealClickListener;

    ListViewHolder(View view, Picasso picasso, DealsAdapter.DealClickedListener dealClickListener) {
        super(view);
        this.picasso = picasso;
        this.dealClickListener = dealClickListener;
        ButterKnife.bind(this, view);
    }

    @Override public void render(DealRenderModel model) {
        picasso.load(model.imageUrl()).into(image);
        title.setText(model.title());
        price.setText(model.price());
        aisle.setText(model.aisle());
        itemView.setTag(model.deal());
    }

    @OnClick(R.id.deal_list_item) void gotoDetail(View view) {
        image.setId(R.id.deal_item_image);
        ViewCompat.setTransitionName(image, dealDetailTransition);
        dealClickListener.onDealClicked((Deal) view.getTag());
    }

    @OnClick(R.id.deal_list_item_aisle) void showOnStoreMap(View view) {
        dealClickListener.onDealAisleClicked((Deal) view.getTag());
    }

    @OnClick(R.id.deal_list_item_ship) void shipIt(View view) {
        dealClickListener.onDealShippedClicked((Deal) view.getTag());
    }
}