package com.target.dealbrowserpoc.dealbrowser.data;

import android.app.Application;
import android.content.SharedPreferences;
import com.f2prateek.rx.preferences.RxSharedPreferences;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.target.dealbrowserpoc.dealbrowser.data.api.ApiModule;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import dagger.Module;
import dagger.Provides;
import java.io.File;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import timber.log.Timber;

import static android.content.Context.MODE_PRIVATE;
import static com.jakewharton.byteunits.DecimalByteUnit.MEGABYTES;

@Module(includes = { ApiModule.class }) public final class DataModule {
    private static final int DISK_CACHE_SIZE = (int) MEGABYTES.toBytes(50);

    static OkHttpClient.Builder createOkHttpClient(Application app) {
        //Install a HTTP cache in the application cache directory.
        File cacheDir = new File(app.getCacheDir(), "http");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);

        return new OkHttpClient.Builder().cache(cache);
    }

    @Provides @ApplicationScope SharedPreferences provideSharedPreferences(Application app) {
        return app.getSharedPreferences("deal_browser", MODE_PRIVATE);
    }

    @Provides @ApplicationScope RxSharedPreferences provideRxSharedPreferences(SharedPreferences prefs) {
        return RxSharedPreferences.create(prefs);
    }

    @Provides @ApplicationScope Picasso providePicasso(Application app, OkHttpClient client) {
        return new Picasso.Builder(app).downloader(new OkHttp3Downloader(client))
                .listener((picasso, uri, e) -> Timber.e(e, "Failed to load image: %s", uri))
                .build();
    }
}