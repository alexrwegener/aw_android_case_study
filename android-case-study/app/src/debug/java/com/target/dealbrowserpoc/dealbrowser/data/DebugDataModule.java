package com.target.dealbrowserpoc.dealbrowser.data;

import android.app.Application;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

@Module public class DebugDataModule {

    @Provides @ApplicationScope HttpLoggingInterceptor provideLoggingInterceptor() {
        //Normally log level would be configurable at runtime
        return new HttpLoggingInterceptor(Timber::d).setLevel(HttpLoggingInterceptor.Level.BASIC);
    }

    @Provides @ApplicationScope OkHttpClient provideOkHttpClient(Application app, HttpLoggingInterceptor loggingInterceptor) {
        return DataModule.createOkHttpClient(app).addInterceptor(loggingInterceptor).build();
    }
}
