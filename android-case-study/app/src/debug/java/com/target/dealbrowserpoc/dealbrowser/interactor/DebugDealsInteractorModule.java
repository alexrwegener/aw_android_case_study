package com.target.dealbrowserpoc.dealbrowser.interactor;

import com.target.dealbrowserpoc.dealbrowser.data.api.deals.DealsService;
import com.target.dealbrowserpoc.dealbrowser.data.store.DealsStore;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import com.target.dealbrowserpoc.dealbrowser.interactor.deal.DealsInteractor;
import com.target.dealbrowserpoc.dealbrowser.interactor.deal.RealDealsInteractor;
import dagger.Module;
import dagger.Provides;

@Module public final class DebugDealsInteractorModule {
    @Provides @ApplicationScope public DealsInteractor provideDealsInteractor(DealsService dealsService, DealsStore dealsStore) {
        //TODO Add fake mode
        return new RealDealsInteractor(dealsService, dealsStore);
    }
}
