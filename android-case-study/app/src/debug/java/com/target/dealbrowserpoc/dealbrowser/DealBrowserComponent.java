package com.target.dealbrowserpoc.dealbrowser;

import com.target.dealbrowserpoc.dealbrowser.data.DebugDataModule;
import com.target.dealbrowserpoc.dealbrowser.di.ApplicationScope;
import com.target.dealbrowserpoc.dealbrowser.interactor.DebugDealsInteractorModule;
import com.target.dealbrowserpoc.dealbrowser.leakcanary.LeakCanaryModule;
import dagger.Component;

@ApplicationScope @Component(modules = { DealBrowserModule.class, LeakCanaryModule.class, DebugDealsInteractorModule.class, DebugDataModule.class })
public interface DealBrowserComponent extends AppComponent {
    final class Initializer {
        private Initializer() {
            //No op
        }

        static DealBrowserComponent init(DealBrowserApp app) {
            return DaggerDealBrowserComponent.builder().dealBrowserModule(new DealBrowserModule(app)).build();
        }
    }
}
