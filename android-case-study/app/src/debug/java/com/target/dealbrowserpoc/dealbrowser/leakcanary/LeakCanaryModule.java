package com.target.dealbrowserpoc.dealbrowser.leakcanary;

import android.app.Application;
import com.target.dealbrowserpoc.dealbrowser.data.MemoryLeakDetector;
import dagger.Module;
import dagger.Provides;

@Module public final class LeakCanaryModule {
    @Provides MemoryLeakDetector registerCanary(Application app) {
        return new LeakCanaryRegister(app).detector();
    }
}