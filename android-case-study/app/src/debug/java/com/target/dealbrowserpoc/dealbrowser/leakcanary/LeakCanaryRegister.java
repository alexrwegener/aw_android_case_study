package com.target.dealbrowserpoc.dealbrowser.leakcanary;

import android.app.Application;
import com.squareup.leakcanary.AndroidExcludedRefs;
import com.squareup.leakcanary.ExcludedRefs;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.target.dealbrowserpoc.dealbrowser.data.MemoryLeakDetector;

public final class LeakCanaryRegister {
    private final MemoryLeakDetector detector;

    LeakCanaryRegister(Application application) {
        //If you have a false positive that you'd like to exclude you would add it via ExcludedRefs
        ExcludedRefs.Builder builder = AndroidExcludedRefs.createAppDefaults();
        //In a real app you would add an upload service to remotely monitor leaks as they happen.
        //RefWatcher refWatcher = LeakCanary.install(application, SlackLeakUploadService.class, builder.build());
        RefWatcher refWatcher = LeakCanary.install(application);
        detector = refWatcher::watch;
    }

    public MemoryLeakDetector detector() {
        return detector;
    }
}
