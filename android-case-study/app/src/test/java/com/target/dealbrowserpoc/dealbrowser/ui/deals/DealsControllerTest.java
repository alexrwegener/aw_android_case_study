package com.target.dealbrowserpoc.dealbrowser.ui.deals;

import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;
import com.target.dealbrowserpoc.dealbrowser.data.model.DealBrowserMode;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deals;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;
import static java.util.Arrays.asList;

public class DealsControllerTest {

    @Test public void createsRenderModelForEachDeal() {
        Deal deal = Deal.create("id", "imageUrl", "aisle", "description", "guid", "price", "title", "salesPrice");
        Deal deal2 = Deal.create("id", "imageUrl", "aisle", "description", "guid", "price", "title", "salesPrice");
        Deals deals = Deals.create(asList(deal, deal2));
        DealsRenderModel model = DealsController.toRenderModel(deals, DealBrowserMode.LIST);
        assertThat(model.dealBrowserMode()).isEqualTo(DealBrowserMode.LIST);
        assertThat(model.deals().size()).isEqualTo(2);
    }

    @Test public void shouldDisplaySalesPriceWhenItExists() {
        String salesPrice = "8.88";
        Deal deal = Deal.create("id", "imageUrl", "aisle", "description", "guid", "10.88", "title", salesPrice);

        DealRenderModel model = DealsController.toDealRenderModel(deal);
        assertThat(model.price()).isEqualTo(salesPrice);
    }

    @Test public void shouldDisplayPriceWhenSalesPriceDoesNotExist() {
        String price = "10.88";
        Deal deal = Deal.create("id", "imageUrl", "aisle", "description", "guid", price, "title", null);

        DealRenderModel model = DealsController.toDealRenderModel(deal);
        assertThat(model.price()).isEqualTo(price);
    }

    @Test public void shouldTitleizeTitle() {
        String title = "pokemon graphic tee";
        String expected = "Pokemon Graphic Tee";
        Deal deal = Deal.create("id", "imageUrl", "aisle", "description", "guid", "price", title, "salesPrice");

        DealRenderModel model = DealsController.toDealRenderModel(deal);
        assertThat(model.title()).isEqualTo(expected);
    }

    @Test public void shouldCapitalizeAisle() {
        String aisle = "c10";
        String expected = "C10";
        Deal deal = Deal.create("id", "imageUrl", aisle, "description", "guid", "price", "title", "salesPrice");

        DealRenderModel model = DealsController.toDealRenderModel(deal);
        assertThat(model.aisle()).isEqualTo(expected);
    }
}
