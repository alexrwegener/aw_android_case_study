package com.target.dealbrowserpoc.dealbrowser.ui.deal;

import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class DealControllerTest {

    @Test public void shouldDisplaySalesPriceWhenItExists() {
        String salesPrice = "8.88";
        String price = "10.88";
        Deal deal = Deal.create("id", "imageUrl", "aisle", "description", "guid", price, "title", salesPrice);

        DealRenderModel model = DealController.toRenderModel(deal);
        assertThat(model.hasSalePrice()).isTrue();
        assertThat(model.price()).isEqualTo(salesPrice);
        assertThat(model.regularPrice()).isEqualTo(price);
    }

    @Test public void shouldDisplayPriceWhenSalesPriceDoesNotExist() {
        String price = "10.88";
        Deal deal = Deal.create("id", "imageUrl", "aisle", "description", "guid", price, "title", null);

        DealRenderModel model = DealController.toRenderModel(deal);
        assertThat(model.hasSalePrice()).isFalse();
        assertThat(model.price()).isEqualTo(price);
    }

    @Test public void shouldTitleizeTitle() {
        String title = "pokemon graphic tee";
        String expected = "Pokemon Graphic Tee";
        Deal deal = Deal.create("id", "imageUrl", "aisle", "description", "guid", "price", title, "salesPrice");

        DealRenderModel model = DealController.toRenderModel(deal);
        assertThat(model.title()).isEqualTo(expected);
    }
}
