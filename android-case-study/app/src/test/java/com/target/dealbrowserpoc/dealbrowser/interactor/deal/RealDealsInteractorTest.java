package com.target.dealbrowserpoc.dealbrowser.interactor.deal;

import com.target.dealbrowserpoc.dealbrowser.data.api.deals.DealsService;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deal;
import com.target.dealbrowserpoc.dealbrowser.data.model.Deals;
import com.target.dealbrowserpoc.dealbrowser.data.store.DealsStore;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import rx.Observable;
import rx.observers.TestSubscriber;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RealDealsInteractorTest {
    @Mock DealsService dealsService;
    @Mock DealsStore dealsStore;
    private final Deal deal = Deal.create("id", "imageUrl", "aisle", "description", "guid", "price", "title", "salePrice");
    private RealDealsInteractor realDealsInteractor;
    private TestSubscriber<Deals> dealsSub;
    private TestSubscriber<Deal> dealSub;

    @Before public void before() {
        MockitoAnnotations.initMocks(this);
        dealsSub = new TestSubscriber<>();
        dealSub = new TestSubscriber<>();
    }

    @Test public void dealsPullsFromMultipleSourcesAndStoresLocally() {
        Deals deals = Deals.create(singletonList(deal));
        when(dealsService.getDeals()).thenReturn(Observable.just(deals));
        when(dealsStore.deals()).thenReturn(Observable.just(Deals.create(singletonList(deal))));
        realDealsInteractor = new RealDealsInteractor(dealsService, dealsStore);
        realDealsInteractor.deals().subscribe(dealsSub);
        dealsSub.assertValueCount(2);
        verify(dealsStore, times(2)).deals(deals);
    }

    @Test public void activeDealPullsFromStore() {
        when(dealsStore.activeDeal()).thenReturn(Observable.just(deal));
        realDealsInteractor = new RealDealsInteractor(dealsService, dealsStore);
        realDealsInteractor.activeDeal().subscribe(dealSub);
        dealSub.assertValueCount(1);
    }

    @Test public void settingActiveDealStoresLocally() {
        realDealsInteractor = new RealDealsInteractor(dealsService, dealsStore);
        realDealsInteractor.activeDeal(deal);
        verify(dealsStore, times(1)).activeDeal(deal);
    }
}
