package com.target.dealbrowserpoc.dealbrowser.interactor.preference;

import com.target.dealbrowserpoc.dealbrowser.data.model.DealBrowserMode;
import com.target.dealbrowserpoc.dealbrowser.data.store.PreferenceStore;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PreferenceInteractorTest {
    @Mock PreferenceStore preferenceStore;
    private PreferenceInteractor preferenceInteractor;
    private TestSubscriber<DealBrowserMode> sub;

    @Before public void before() {
        MockitoAnnotations.initMocks(this);
        sub = new TestSubscriber<>();
    }

    @Test public void dealBrowserModePullsFromStore() {
        when(preferenceStore.dealBrowserMode()).thenReturn(Observable.just(DealBrowserMode.LIST));
        preferenceInteractor = new PreferenceInteractor(preferenceStore);
        preferenceInteractor.dealBrowserMode().subscribe(sub);
        sub.assertValueCount(1);
    }

    @Test public void togglingBrowserModeStoresLocallyAndToggles() {
        preferenceInteractor = new PreferenceInteractor(preferenceStore);
        preferenceInteractor.toggleDealBrowserMode(DealBrowserMode.GRID);
        verify(preferenceStore, times(1)).dealBrowserMode(DealBrowserMode.LIST);
    }
}
