package com.target.dealbrowserpoc.dealbrowser.data.model;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class DealBrowserModeTest {

    @Test public void listModeTogglesToGrid() {
        assertThat(DealBrowserMode.LIST.toggle()).isEqualTo(DealBrowserMode.GRID);
    }

    @Test public void gridModeTogglesToList() {
        assertThat(DealBrowserMode.GRID.toggle()).isEqualTo(DealBrowserMode.LIST);
    }
}
